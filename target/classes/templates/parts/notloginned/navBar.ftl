<#import 'login.ftl' as login>
<#macro get>
<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
    <a class="navbar-brand" href="/">HomeHelper</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Market</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">My devices</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Help</a>
            </li>
        </ul>
        <form action="/login">
        <button class="btn btn-primary">Log in</button>
        </form>
        <form action="/registration">
        <button class="btn btn-primary ml-2">Registration</button>
        </form>
    </div>
</nav>
</#macro>