<#macro form>
    <form action="/login" method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />﻿
        <div class="form-group">
            <label for="exampleInputEmail1">User name</label>
            <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter user name">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
        </div>
        <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" id="exampleCheck1">
            <label class="form-check-label" for="exampleCheck1">Remember me</label>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</#macro>

<#macro logout>
    <form action="/logout" method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />﻿
        <button type="submit" class="btn btn-primary">Sign Out</button>
    </form>
</#macro>

<#macro reg_form>
    <form action="/registration" method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />﻿
        <div class="form-group">
            <label>User name</label>
            <input type="text" name="username" class="form-control" placeholder="Enter user name">
        </div>
        <div class="form-group">
            <label>Password</label>
            <input type="password" name="password" class="form-control" placeholder="Password">
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" class="form-control" placeholder="email@mail.com">
        </div>
        <div class="form-group">
            <label>Phone number</label>
            <input type="text" name="phoneNum" class="form-control" placeholder="+ XX XXX XXX XXX">
        </div>
        <div class="form-group">
            <label>Station Id</label>
            <input type="number" name="stationId" class="form-control" placeholder="123 456">
        </div>
        <div class="form-group">
            <label>Station password</label>
            <input type="text" name="stationPsw" class="form-control" placeholder="123 456">
        </div>
        <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" id="exampleCheck1">
            <label class="form-check-label" for="exampleCheck1">Remember me</label>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</#macro>