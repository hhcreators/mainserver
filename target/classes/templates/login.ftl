<#import "parts/common.ftl" as comm>
<#import "parts/notloginned/login.ftl" as login>

<@comm.main_wrap>
    <@login.form/>
</@comm.main_wrap>