<#import "../parts/common.ftl" as comm>

<@comm.main_wrap>

<div class="input-group">
    <form action="/admin/userList/filter" method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        <div class="form-group">
            <label>Text filter :</label>
            <input type="text" class="form-control" placeholder="Filter" name="value">
        </div>
        <select class="form-control form-control-sm" name="findBy">
            <option value="username">by username</option>
            <option value="lastName">by last name</option>
            <option value="phoneNum">by phone number</option>
        </select>
        <button type="submit" class="btn btn-primary mt-3">Submit</button>
    </form>
</div>

<#if users?hasContent>
<table class="mx-auto table mt-4">
    <thead class="thead-dark">
    <th scope="col">ID</th>
    <th scope="col">User name</th>
    <th scope="col">Name</th>
    <th scope="col">Last name</th>
    <th scope="col">E-mail</th>
    <th scope="col">Phone</th>
    <th scope="col">Role</th>
    <th scope="col"></th>
    </thead>
<tbody>

    <#list users as user>
        <tr>
            <th scope="row">${user.id!}</th>
            <td>${user.username!}</td>
            <td>${user.firstName!}</td>
            <td>${user.lastName!}</td>
            <td>${user.email!}</td>
            <td>${user.phoneNum!}</td>
            <td><#list user.roles as role>
                    ${role!}<#sep>, </#sep>
                </#list>
            </td>
            <td><a href="/admin/userList/id/${user.id!}">Edit</a> </td>
        </tr>
    </#list>

<#else >
    <span>No users find !</span>
</tbody>
</table>
</#if>
</@comm.main_wrap>