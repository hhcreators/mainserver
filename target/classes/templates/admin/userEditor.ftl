<#import "../parts/common.ftl" as comm>

<@comm.main_wrap>
    <form action="/admin/userList/userEditSave" method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        <table class="mx-auto table mt-4">
            <thead class="thead-dark">
                <th scope="col">ID</th>
                <th scope="col">User name</th>
                <th scope="col">Name</th>
                <th scope="col">Last name</th>
                <th scope="col">Phone</th>
            </thead>
            <tbody>
            <tr>
                <td>
                    <input type="text" value="${user.id!}" name="userId"/>
                </td>
                <td>
                    <input type="text" value="${user.username!}" name="userName"/>
                </td>
                <td>
                    <input type="text" value="${user.firstName!}" name="firstName"/>
                </td>
                <td>
                    <input type="text" value="${user.lastName!}" name="lastName"/>
                </td>
                <td>
                    <input type="text" value="${user.phoneNum!}" name="phoneNum"/>
                </td>
            </tr>
            </tbody>
        </table>
        <#list roles as role>
                <div>
                    <label>
                    <input type="checkbox" name="${role}" ${user.roles?seqContains(role)?string("checked", "")}>${role}
                    </label>
                </div>
        </#list>
        <button type="submit" class="btn btn-primary mt-3">Submit</button>
    </form>
</@comm.main_wrap>