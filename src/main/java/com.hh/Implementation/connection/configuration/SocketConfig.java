package com.hh.Implementation.connection.configuration;

import com.hh.Implementation.connection.controllers.SocketController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.UnknownHostException;

@Configuration
public class SocketConfig {

    @Bean
    public SocketController getSocket(){
        try {
            SocketController socket = new SocketController(13820);
            socket.start();
            System.err.println("Socket started !");
            return socket;
        } catch (UnknownHostException e) {
            return null;
        }
    }

}
