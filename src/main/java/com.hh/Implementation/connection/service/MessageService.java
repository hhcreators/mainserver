package com.hh.Implementation.connection.service;

import com.hh.Implementation.DAO.entity.Device;
import com.hh.Implementation.DAO.entity.DevicesValues;
import com.hh.Implementation.DAO.entity.Station;
import com.hh.Implementation.DAO.repos.DevicesRepo;
import com.hh.Implementation.DAO.repos.DevicesValuesRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class MessageService {

    private final DevicesRepo devicesRepo;
    private final DevicesValuesRepo devicesValuesRepo;

    @Autowired
    public MessageService(DevicesRepo devicesRepo, DevicesValuesRepo devicesValuesRepo) {
        this.devicesRepo = devicesRepo;
        this.devicesValuesRepo = devicesValuesRepo;
    }

    public boolean currentValueToDB(Long deviceId, String deviceValue){

        Device device;
        Optional<Device> deviceOptional = devicesRepo.findById(deviceId);

        if (deviceOptional.isPresent()) {
            device = deviceOptional.get();
            device.setCurrentValue(deviceValue);
            DevicesValues devicesValues = new DevicesValues();
            devicesValues.setDevice(device);
            devicesValues.setSensorValue(deviceValue);
            devicesValues.setUpdateTime(new Date());
            devicesRepo.save(device);
            devicesValuesRepo.save(devicesValues);
            System.err.println("device values accepted");
            return true;
            }
            return false;
    }
}
