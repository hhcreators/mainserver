package com.hh.Implementation.connection.service;

import com.hh.Implementation.connection.clients.Client;
import com.hh.Interfaces.ClientHandlerInt;
import org.java_websocket.WebSocket;
import org.springframework.stereotype.Service;

import java.net.InetSocketAddress;
import java.util.ArrayList;


@Service
public class ClientService implements ClientHandlerInt {

    private static ArrayList<Client> clients = new ArrayList<>();

    public ClientService(){

    }


    public static boolean addUser(WebSocket socket) {
        Client client = new Client(socket);
        return clients.add(client);
    }


    public static boolean deleteUser(WebSocket socket) {
        for (Client c : clients) {
            if (c.getIP().contains(socket.getRemoteSocketAddress().getAddress().getHostAddress())) {
                clients.remove(c);
                return true;
            }
        }
        return false;
    }


    public static Client getUser(long clientID) {
        Client client = null;
        for (Client c : clients) {
            if (c.getID() == clientID) client = c;
        }
        return client;
    }


    public static Client getUser(InetSocketAddress inetSocketAddress) {
        Client client = null;
        for (Client c : clients) {
            if (c.getInetSocketAddress().toString().equals(inetSocketAddress.toString())) client = c;
        }
        return client;
    }

}
