package com.hh.Implementation.connection.controllers;

import com.hh.Implementation.connection.service.ClientService;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

@Controller
public class SocketController extends WebSocketServer {

        @Autowired
        private MessageController massegeHandler;

        public SocketController() throws UnknownHostException {
        }

        public SocketController(int port ) throws UnknownHostException {
                super( new InetSocketAddress( port ) );
        }

        public SocketController(InetSocketAddress address ) {
            super( address );
        }
        @OnOpen
        @Override
        public void onOpen(WebSocket conn, ClientHandshake handshake ) {
            System.err.println( conn.getRemoteSocketAddress().getAddress().getHostAddress() + " connected to server!" );
            ClientService.addUser(conn);
        }
        @OnClose
        @Override
        public void onClose( WebSocket conn, int code, String reason, boolean remote ) {
            System.err.println( conn.getRemoteSocketAddress().getAddress().getHostAddress() + " has left the server!" );
            ClientService.deleteUser(conn);

        }
        @OnMessage
        @Override
        public void onMessage( WebSocket conn, String message ) {
            System.err.println("GET: " + conn.getRemoteSocketAddress().getAddress().getHostAddress() + ": " + message );
            massegeHandler.processMessage(message, conn);
        }
        @Override
        public void onMessage( WebSocket conn, ByteBuffer message ) {
            onMessage(conn, new String(message.array()));
        }

        @OnError
        @Override
        public void onError( WebSocket conn, Exception ex ) {
            System.err.print("Socket error :");
            ex.printStackTrace();
            ex.getCause();
            if( conn != null ) {
                sendMassage(conn,ex.toString());
            }
        }

        public void sendMassage(WebSocket conn, String message){
            conn.send(message);
            System.err.println("SEND: " + conn.getRemoteSocketAddress().getAddress().getHostAddress() + ": " + message );
        }

    }
