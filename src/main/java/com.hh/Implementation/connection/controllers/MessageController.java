package com.hh.Implementation.connection.controllers;

import com.hh.Implementation.connection.clients.Client;
import com.hh.Implementation.connection.service.ClientService;
import com.hh.Implementation.connection.service.MessageService;
import com.hh.Interfaces.MessageHandlerInt;
import org.java_websocket.WebSocket;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class MessageController implements MessageHandlerInt {

    //TYPES
    //INIT - type to register new device on server
    //RFS - response from sensor

    private final MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService){
        this.messageService = messageService;
    }

    @Override
    public boolean processMessage(String message, WebSocket socket) {
        JSONObject jsonObject;
        Client client = ClientService.getUser(socket.getRemoteSocketAddress());
        try {
            jsonObject = new JSONObject(message);
            String type = jsonObject.getString("type");
            if(type.equals("INIT")) {
                client.setID(Integer.parseInt(jsonObject.getString("ID")));
                return true;
            }
            //{"stID":777,"dvID":123,"dvTyp":"","dvVal":"t32.00h46.00"}
            if (type.equals("RFS")) {
                //TODO
                long stationID = jsonObject.getLong("stID");
                long deviceID = jsonObject.getLong("dvID");
                String deviceType = jsonObject.getString("dvTyp");
                String deviceValue  = jsonObject.getString("dvVal");
                if (deviceType.equals("SENSOR")){
                    deviceValue = "temp: " + deviceValue.substring(deviceValue.indexOf("t") + 1, 6)
                            + " hum: "
                            + deviceValue.substring(deviceValue.indexOf("h") + 1);
                }
                messageService.currentValueToDB(deviceID, deviceValue);
                return true;
            }

        }catch (Exception e){
            e.printStackTrace();
            System.out.println(socket.getRemoteSocketAddress().getAddress().toString()+ ": " + message);
        }
        return false;
    }

}
