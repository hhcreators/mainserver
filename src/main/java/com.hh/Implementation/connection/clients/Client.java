package com.hh.Implementation.connection.clients;

import com.hh.Interfaces.ClientInt;
import org.java_websocket.WebSocket;

import java.net.InetSocketAddress;


public class Client implements ClientInt{

    private WebSocket socket;
    private long ID = 0;

    public Client(WebSocket socket){
        this.socket = socket;
    }

    @Override
    public InetSocketAddress getInetSocketAddress() {
        return socket.getRemoteSocketAddress();
    }

    @Override
    public String getIP() {
        return socket.getRemoteSocketAddress().getAddress().getHostAddress();
    }

    @Override
    public long getID() {
        return ID;
    }

    @Override
    public boolean setID(long id) {
        this.ID=id;
        return true;
    }

    @Override
    public void closeConnection() {
        socket.close();
    }

}
