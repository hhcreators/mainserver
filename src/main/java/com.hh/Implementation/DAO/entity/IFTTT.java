package com.hh.Implementation.DAO.entity;

import javax.persistence.*;

@Entity
@Table(name = "ifttt", schema = "hh")
public class IFTTT {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Basic
    @Column(name = "ifttt_name")
    private String iftttName;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "this_id")
    private Device thisDevice;

    @Basic
    @Column(name = "this_value")
    private String thisValue;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "that_id")
    private Device thatDevice;

    @Basic
    @Column(name = "that_value")
    private String thatValue;

    public IFTTT() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIftttName() {
        return iftttName;
    }

    public void setIftttName(String iftttName) {
        this.iftttName = iftttName;
    }

    public Device getThisDevice() {
        return thisDevice;
    }

    public void setThisDevice(Device thisDevice) {
        this.thisDevice = thisDevice;
    }

    public String getThisValue() {
        return thisValue;
    }

    public void setThisValue(String thisValue) {
        this.thisValue = thisValue;
    }

    public Device getThatDevice() {
        return thatDevice;
    }

    public void setThatDevice(Device thatDevice) {
        this.thatDevice = thatDevice;
    }

    public String getThatValue() {
        return thatValue;
    }

    public void setThatValue(String thatValue) {
        this.thatValue = thatValue;
    }
}
