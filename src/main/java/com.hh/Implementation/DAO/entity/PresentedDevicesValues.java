package com.hh.Implementation.DAO.entity;

import javax.persistence.*;

@Entity
@Table(name = "presented_devices_values", schema = "hh")
public class PresentedDevicesValues {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "presented_device_id")
    private PresentedDevices presentedDevices;

    @Basic
    @Column(name = "device_value" )
    private String deviceValue;

    public PresentedDevicesValues() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PresentedDevices getPresentedDevices() {
        return presentedDevices;
    }

    public void setPresentedDevices(PresentedDevices presentedDevices) {
        this.presentedDevices = presentedDevices;
    }

    public String getDeviceValue() {
        return deviceValue;
    }

    public void setDeviceValue(String deviceValue) {
        this.deviceValue = deviceValue;
    }
}
