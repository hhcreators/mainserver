package com.hh.Implementation.DAO.entity;

import javax.persistence.*;

@Entity
@Table(name = "presented_devices", schema = "hh")
public class PresentedDevices {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Basic
    @Column(name = "device_type")
    private String deviceType;

    @Basic
    @Column(name = "device_sub_type" )
    private String deviceSubType;

    public PresentedDevices() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceSubType() {
        return deviceSubType;
    }

    public void setDeviceSubType(String deviceSubType) {
        this.deviceSubType = deviceSubType;
    }
}
