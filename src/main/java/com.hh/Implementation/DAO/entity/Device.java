package com.hh.Implementation.DAO.entity;

import javax.persistence.*;

@Entity
@Table(name = "devices", schema = "hh")
public class Device {
    @Id
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "station_id")
    private Station station;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "presented_device_id")
    private PresentedDevices presentedDevices;

    @Basic
    @Column(name = "device_name")
    private String deviceName;

    @Basic
    @Column(name = "current_value")
    private String currentValue;

    public Device() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public PresentedDevices getPresentedDevices() {
        return presentedDevices;
    }

    public void setPresentedDevices(PresentedDevices presentedDevices) {
        this.presentedDevices = presentedDevices;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(String currentValue) {
        this.currentValue = currentValue;
    }
}
