package com.hh.Implementation.DAO.entity;

import javax.persistence.*;

@Entity
@Table(name = "station", schema = "hh")
public class Station {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "station_type")
    private String stationType;

    @Column(name = "station_mac")
    private String mac;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "station_password")
    private String stationPassword;


    public Station() {
    }

    public Station(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getStationType() {
        return stationType;
    }

    public void setStationType(String stationType) {
        this.stationType = stationType;
    }

    public String getStationPassword() {
        return stationPassword;
    }

    public void setStationPassword(String stationPassword) {
        this.stationPassword = stationPassword;
    }
}
