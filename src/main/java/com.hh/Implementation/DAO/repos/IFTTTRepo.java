package com.hh.Implementation.DAO.repos;

import com.hh.Implementation.DAO.entity.Device;
import com.hh.Implementation.DAO.entity.IFTTT;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface IFTTTRepo extends CrudRepository<IFTTT, Device> {

    Optional<IFTTT> findById(Long id);

    List<IFTTT> findByThisDeviceId(Long deviceId);

    List<IFTTT> findByThatDeviceId(Long deviceId);

    List<IFTTT> findByThisDeviceIdAndThatDeviceId(Long thisDeviceId, Long thatDeviceId);

    List<IFTTT> findByThisDeviceIdOrThatDeviceId(Long thisDeviceId, Long thatDeviceId);

    List<IFTTT> findByIftttName(String iftttName);

}