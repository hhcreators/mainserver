package com.hh.Implementation.DAO.repos;

import com.hh.Implementation.DAO.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepo extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

    List<User> findByLastName(String lastName);

    List<User> findByPhoneNum(String phoneNum);

    Optional<User> findByActivationCode(String code);
}
