package com.hh.Implementation.DAO.repos;

import com.hh.Implementation.DAO.entity.Device;
import com.hh.Implementation.DAO.entity.Station;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface DevicesRepo extends CrudRepository<Device, java.lang.Long> {

    Optional<Device> findById(Long id);

    List<Device> findByStationId(Long stationID);

    Optional<Device> findAllByIdAndStationId(Station stationID, Long deviceID);
}