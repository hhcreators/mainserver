package com.hh.Implementation.DAO.repos;

import com.hh.Implementation.DAO.entity.DevicesValues;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DevicesValuesRepo extends CrudRepository<DevicesValues, Long> {

    List<DevicesValues> findByDeviceId(Long id);


}