package com.hh.Implementation.DAO.repos;

import com.hh.Implementation.DAO.entity.PresentedDevices;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface PresentedDevicesRepo extends CrudRepository<PresentedDevices, Long> {

    Optional<PresentedDevices> findById(Long id);
    List<PresentedDevices> findByDeviceType(String type);
    Optional<PresentedDevices> findByDeviceTypeAndDeviceSubType(String type, String subType);

}