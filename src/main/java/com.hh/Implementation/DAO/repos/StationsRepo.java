package com.hh.Implementation.DAO.repos;

import com.hh.Implementation.DAO.entity.Station;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface StationsRepo extends CrudRepository<Station, Long> {

    List<Station> findByUserId(Long id);
}