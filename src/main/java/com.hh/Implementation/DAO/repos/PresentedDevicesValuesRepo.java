package com.hh.Implementation.DAO.repos;

import com.hh.Implementation.DAO.entity.PresentedDevicesValues;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface PresentedDevicesValuesRepo extends CrudRepository<PresentedDevicesValues, Long> {

    Optional<PresentedDevicesValues> findById(Long id);
    List<PresentedDevicesValues> findByPresentedDevicesId(Long presentedDeviceId);

}