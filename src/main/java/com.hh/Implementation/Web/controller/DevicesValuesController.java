package com.hh.Implementation.Web.controller;

import com.hh.Implementation.DAO.entity.Device;
import com.hh.Implementation.DAO.entity.User;
import com.hh.Implementation.DAO.repos.DevicesValuesRepo;
import com.hh.Implementation.Web.service.DevicesValuesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.Map;

@RequestMapping("/devicesValues")
@Controller
public class DevicesValuesController {

    private final DevicesValuesRepo devicesValuesRepo;
    private final DevicesValuesService devicesValuesService;


    @Autowired
    public DevicesValuesController( DevicesValuesRepo devicesValuesRepo, DevicesValuesService devicesValuesService) {
        this.devicesValuesRepo = devicesValuesRepo;
        this.devicesValuesService = devicesValuesService;
    }

    @GetMapping
    public String listDevicesAndValues(
            @AuthenticationPrincipal User user,
            Map<String, Object> model){
        model.putAll(devicesValuesService.myDevicesList(user));

        return "deviceList";
    }

    @GetMapping("/detail/{device}")
    public String deviceDetails(
            @PathVariable Device device,
            Map<String, Object> model){

        model.put("deviceName", device.getDeviceName());
        model.put("deviceValues", devicesValuesRepo.findByDeviceId(device.getId()));

        return "deviceValuesList";
    }


    @GetMapping("/changeState")
    public String changeDeviceState(
            @RequestParam("state") String state,
            @RequestParam("device") Device device){
        devicesValuesService.changeDeviceValue(state, device);

        return "/devicesValues";
    }
}
