package com.hh.Implementation.Web.controller;

import com.hh.Implementation.DAO.entity.User;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

@Controller
public class MainController {

    @GetMapping("/")
    String main (Map<String, Object> model) {
        return "storage";
    }

    @GetMapping("/login")
    public String login(@AuthenticationPrincipal User user){
       if (user == null) {
           return "login";
       }else {
           return "storage";
       }
    }




}