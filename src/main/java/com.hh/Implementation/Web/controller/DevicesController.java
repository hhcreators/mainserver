package com.hh.Implementation.Web.controller;


import com.hh.Implementation.DAO.entity.Station;
import com.hh.Implementation.DAO.entity.User;
import com.hh.Implementation.Web.service.DevicesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class DevicesController {

    private final DevicesService devicesService;

    @Autowired
    public DevicesController(DevicesService devicesService) {
        this.devicesService = devicesService;
    }

    @GetMapping("/stations")
    public String myStations(
            @AuthenticationPrincipal User user,
            Map<String, Object> model){

        model.putAll(devicesService.showMyStations(user));

        return "stations";
    }


    @PostMapping("new_station")
    public String newStation(
            @AuthenticationPrincipal User user,
            @RequestParam("station_id")Station station,
            @RequestParam("stationPsw")String stationPassword,
            Map<String, Object> model) {
        List<String> messages = new ArrayList<>();
        if (devicesService.addNewStation(user, station, stationPassword)){
            messages.add("Station add !");
        }else {
            messages.add("Can't add station");
        }
        model.put("messages", messages);

        return myStations(user, model);
    }


    @GetMapping("/sensors")
    public String mySensors(
            @AuthenticationPrincipal User user,
            Map<String, Object> model){

        model.putAll(devicesService.showMySensors(user));
        return "sensors";
    }


    @GetMapping("/controllers")
    public String myControllers(
            @AuthenticationPrincipal User user,
            Map<String, Object> model){

        model.putAll(devicesService.showMyControllers(user));
        return "controllers";
    }

}
