package com.hh.Implementation.Web.controller;

import com.hh.Implementation.DAO.entity.Device;
import com.hh.Implementation.DAO.entity.User;
import com.hh.Implementation.Web.service.ScenarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RequestMapping("/scenario")
@Controller
public class ScenarioController {

    private final ScenarioService scenarioService;

    @Autowired
    public ScenarioController(ScenarioService scenarioService) {
        this.scenarioService = scenarioService;
    }

    @GetMapping
    public String scenario(@AuthenticationPrincipal User user,
                           Map<String, Object> model){
        model.putAll(scenarioService.showMyScenarios(user));

        return "ifttt";
    }

    @GetMapping("/new")
    public String newScenario(Map<String, Object> model,
                              @AuthenticationPrincipal User user){
        model.putAll(scenarioService.showCreationForm(user));
        return "iftttNew";
    }

    @PostMapping("/new/create")
    public String createScenario(
            @RequestParam("scenarioName") String name,
            @RequestParam("scenarioThisDevice") Device thisDevice,
            @RequestParam("scenarioThisValue") String thisValue,
            @RequestParam("scenarioThatDevice") Device thatDevice,
            @RequestParam("scenarioThatValue") String thatValue,
            @AuthenticationPrincipal User user,
            Map<String, Object> model) {
        //TODO check enter rows

        if(scenarioService.createScenario(name, thisDevice, thisValue, thatDevice, thatValue)){
            //TODO
        }
        model.putAll(scenarioService.showMyScenarios(user));
        return "ifttt";
    }

    @GetMapping("/edit/{iftttId}")
    public String editScenario(@AuthenticationPrincipal User user,
                               Map<String, Object> model,
                               @PathVariable Long iftttId){
        model.putAll(scenarioService.showEditForm(user, iftttId));
        return "iftttEdit";
    }

    @PostMapping("/edit/{iftttId}")
    public String updateScenario(@RequestParam("iftttId") Long iftttId,
                                 @RequestParam("scenarioName") String scenarioName,
                                 @RequestParam("scenarioThisDevice") Device scenarioThisDevice,
                                 @RequestParam("scenarioThisValue") String scenarioThisValue,
                                 @RequestParam("scenarioThatDevice") Device scenarioThatDevice,
                                 @RequestParam("scenarioThatValue") String scenarioThatValue){
        //TODO check for inputs
        if(scenarioService.updateScenario(iftttId, scenarioName, scenarioThisDevice, scenarioThisValue, scenarioThatDevice, scenarioThatValue)){

        }else {
            //TODO
        }
        return "redirect:/scenario";
    }
}
