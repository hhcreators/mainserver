package com.hh.Implementation.Web.controller;

import com.hh.Implementation.DAO.entity.Role;
import com.hh.Implementation.DAO.entity.User;
import com.hh.Implementation.DAO.repos.UserRepo;
import com.hh.Implementation.Web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("admin/userList")
@PreAuthorize("hasAuthority('MAIN_ADMIN')")
public class UserController {

    private final UserRepo userRepo;
    private final UserService userService;

    @Autowired
    public UserController(UserRepo userRepo, UserService userService) {
        this.userRepo = userRepo;
        this.userService = userService;
    }

    @GetMapping
    public String userList(Map<String, Object> model){
        model.put("users", userRepo.findAll());
        return "admin/userList";
    }

    @GetMapping("/id/{user}")
    public String userEdit(@PathVariable User user, Map<String, Object> model){
        model.put("user", user);
        model.put("roles", Role.values());
        return "admin/userEditor";
    }

    @PostMapping("/filter")
    public String filter(@RequestParam("findBy") String findBy,
                         @RequestParam("value") String value,
                         Map<String, Object> model){

        model.putAll(userService.filterByValue(findBy, value));

        return "admin/userList";
    }

    @PostMapping("userEditSave")
    public String userEditSave(@RequestParam("userId") User user,
                               @RequestParam("userName") String userName,
                               @RequestParam("firstName") String firstName,
                               @RequestParam("lastName") String lastName,
                               @RequestParam("phoneNum") String phoneNum,
                               @RequestParam Map<String, String> form){
        if (userService.saveUserEdit(user, userName, firstName, lastName, phoneNum, form)){
            //TODO if and else does not work if we use redirect
        }else{

        }

        return "redirect:/admin/userList";
    }



}
