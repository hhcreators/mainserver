package com.hh.Implementation.Web.controller;

import com.hh.Implementation.DAO.entity.Station;
import com.hh.Implementation.DAO.entity.User;
import com.hh.Implementation.Web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class RegistrationController {

    private final UserService userService;

    @Autowired
    public RegistrationController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/registration")
    public String registration(@AuthenticationPrincipal User user){
        if (user == null) {
            return "registration";
        }else {
            return "storage";
        }
    }

    @PostMapping("/registration")
    public String addUser(User user,
                          @RequestParam("stationId")Station station,
                          @RequestParam("stationPsw")String stationPsw,
                          Map<String, Object> model) {
        List<String> messages = new ArrayList<>();
        if (!userService.addUser(user , station, stationPsw)) {
            messages.add("Something wrong !");
            model.put("messages", messages);
            return "registration";
        }else {
            messages.add("Registration success!");
            model.put("messages", messages);
            return "login";
        }
    }

    @GetMapping("/activate/{code}")
    public String activateAccount(@PathVariable String code,
                                  Map<String, Object> model){
        List<String> messages = new ArrayList<>();
        if(userService.activateUser(code)){
            messages.add("User activated !");
        }else {
            messages.add("Wrong activation code !");
        }
        model.put("messages", messages);
        return "login";
    }
}
