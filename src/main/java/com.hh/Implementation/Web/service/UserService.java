package com.hh.Implementation.Web.service;

import com.hh.Implementation.DAO.entity.Role;
import com.hh.Implementation.DAO.entity.Station;
import com.hh.Implementation.DAO.entity.User;
import com.hh.Implementation.DAO.repos.StationsRepo;
import com.hh.Implementation.DAO.repos.UserRepo;
import com.mysql.jdbc.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService implements UserDetailsService {

    private final UserRepo userRepo;
    private final MailService mailService;
    private final StationsRepo stationsRepo;
    private final PasswordEncoder passwordEncoder;

    @Value("${activation.url}")
    private String activationPageURL;

    @Autowired
    public UserService(UserRepo userRepo,
                       MailService mailService,
                       StationsRepo stationsRepo,
                       @Lazy PasswordEncoder passwordEncoder){
        this.userRepo = userRepo;
        this.mailService = mailService;
        this.stationsRepo = stationsRepo;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Optional<User> user =  userRepo.findByUsername(userName);

        if (!user.isPresent()) {
            throw new UsernameNotFoundException("User not authorized.");
        }else {
            return user.get();
        }
    }

    public boolean addUser(User user, Station station, String stationPassword){
            if (user.getId() == null && station != null) {
                if (station.getStationPassword().equals(stationPassword) && station.getUser() == null) {
                    user.setActive(true);
                    user.setRoles(Collections.singleton(Role.MAIN_ADMIN));
                    user.setActivationCode(UUID.randomUUID().toString());
                    user.setPassword(passwordEncoder.encode(user.getPassword()));
                    userRepo.save(user);
                    station.setUser(user);
                    stationsRepo.save(station);
                    if (!StringUtils.isNullOrEmpty(user.getEmail())){
                        String message = String.format(
                                "Hello, %s ! \n" +
                                        "Welcome to HomeHelper, please activate your account by follow link " + activationPageURL + "%s",
                                user.getUsername(),
                                user.getActivationCode()
                        );
                        mailService.send(user.getEmail(), "Activation code", message);

                    }
                }else {
                    return false;
                }
                return true;
            }else {
                return false;
            }
    }

    public boolean activateUser(String code) {
        Optional<User> userOptional = userRepo.findByActivationCode(code);
        if (userOptional.isPresent()){
            User user = userOptional.get();
            user.setActivationCode(null);
            userRepo.save(user);
            return true;
        }else {
            return false;
        }
    }


    public Map<String, Object> filterByValue(String findBy, String value) {
        Map<String, Object> model = new HashMap<>();

        if (findBy.equals("username")) {
            Optional<User> userOptional = userRepo.findByUsername(value);
            if (userOptional.isPresent()) {
                ArrayList<User> users = new ArrayList<>();
                users.add(userOptional.get());
                model.put("users", users);
            }
        }
        if (findBy.equals("lastName")) {
            model.put("users", userRepo.findByLastName(value));
        }
        if (findBy.equals("phoneNum")) {
            model.put("users", userRepo.findByPhoneNum(value));
        }

        model.put("roles", Role.values());

        return model;
    }

    public boolean saveUserEdit(User user, String userName, String firstName, String lastName, String phoneNum, Map<String, String> form) {
        user.setUsername(userName);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setPhoneNum(phoneNum);

        Set<String> roles = Arrays.stream(Role.values())
                .map(Role::name)
                .collect(Collectors.toSet());

        user.getRoles().clear();

        for (String key : form.keySet()) {
            if (roles.contains(key)) {
                user.getRoles().add(Role.valueOf(key));
            }
        }

        user.setId(user.getId());

        userRepo.save(user);
        return true;
    }
}
