package com.hh.Implementation.Web.service;

import com.hh.Implementation.DAO.entity.Device;
import com.hh.Implementation.DAO.entity.PresentedDevices;
import com.hh.Implementation.DAO.entity.Station;
import com.hh.Implementation.DAO.entity.User;
import com.hh.Implementation.DAO.repos.DevicesRepo;
import com.hh.Implementation.DAO.repos.PresentedDevicesRepo;
import com.hh.Implementation.DAO.repos.StationsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DevicesService {

    private final StationsRepo stationsRepo;
    private final DevicesRepo devicesRepo;
    private final PresentedDevicesRepo presentedDevicesRepo;

    @Autowired
    public DevicesService(StationsRepo stationsRepo, DevicesRepo devicesRepo, PresentedDevicesRepo presentedDevicesRepo) {
        this.stationsRepo = stationsRepo;
        this.devicesRepo = devicesRepo;
        this.presentedDevicesRepo = presentedDevicesRepo;
    }

    public Map<String, Object> showMyStations(User user){
        Map<String, Object> model = new HashMap<>();
        Iterable<Station> stations = stationsRepo.findByUserId(user.getId());
        model.put("stations", stations);
        return model;
    }

    public boolean addNewStation(User user, Station station, String stationPassword){
        if (station.getUser() == null && station.getStationPassword().equals(stationPassword)){
                station.setUser(user);
                stationsRepo.save(station);
                return true;
        }else {
            return false;
        }
    }

    public Map<String, Object> showMySensors(User user){

        Map<String, Object> model = new HashMap<>();
        Iterable<Station> stations = stationsRepo.findByUserId(user.getId());
        List<PresentedDevices> ps = (List<PresentedDevices>) presentedDevicesRepo.findAll();
        List<Device> devices = new ArrayList<>();
        for (Station st : stations) {
            List<Device> deviceList = devicesRepo.findByStationId(st.getId());
            for (Device dv : deviceList){
                for (PresentedDevices pd : ps) {
                    if (dv.getPresentedDevices().getId().equals(pd.getId()) && pd.getDeviceType().equals("SENSOR")){
                        devices.add(dv);
                    }
                }
            }
        }
        model.put("sensors", devices);
        return model;
    }

    public Map<String, Object> showMyControllers(User user) {
        Map<String, Object> model = new HashMap<>();
        Iterable<Station> stations = stationsRepo.findByUserId(user.getId());
        List<PresentedDevices> ps = (List<PresentedDevices>) presentedDevicesRepo.findAll();
        List<Device> devices = new ArrayList<>();
        for (Station st : stations) {
            List<Device> deviceList = devicesRepo.findByStationId(st.getId());
            for (Device dv : deviceList){
                for (PresentedDevices pd : ps) {
                    if (dv.getPresentedDevices().getId().equals(pd.getId()) && pd.getDeviceType().equals("CONTROLLER")){
                        devices.add(dv);
                    }
                }
            }
        }
        model.put("controllers", devices);
        return model;
    }
}
