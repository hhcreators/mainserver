package com.hh.Implementation.Web.service;

import com.hh.Implementation.DAO.entity.Device;
import com.hh.Implementation.DAO.entity.DevicesValues;
import com.hh.Implementation.DAO.entity.Station;
import com.hh.Implementation.DAO.entity.User;
import com.hh.Implementation.DAO.repos.DevicesRepo;
import com.hh.Implementation.DAO.repos.DevicesValuesRepo;
import com.hh.Implementation.DAO.repos.StationsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DevicesValuesService {


    private final DevicesRepo devicesRepo;
    private final StationsRepo stationsRepo;
    private final DevicesValuesRepo devicesValuesRepo;

    @Autowired
    public DevicesValuesService(DevicesRepo devicesRepo, StationsRepo stationsRepo, DevicesValuesRepo devicesValuesRepo) {
        this.devicesRepo = devicesRepo;
        this.stationsRepo = stationsRepo;
        this.devicesValuesRepo = devicesValuesRepo;
    }

    public Map<String, Object> myDevicesList(User user) {
    Map<String, Object> model = new HashMap<>();
    Iterable<Station> stations = stationsRepo.findByUserId(user.getId());
    List<Device> devices = new ArrayList<>();
    for (Station st : stations) {
        List<Device> deviceTemp = devicesRepo.findByStationId(st.getId());
        devices.addAll(deviceTemp);
    }
    model.put("devices", devices);

    return model;
    }

    public boolean changeDeviceValue(String state, Device device) {
        state = state.toUpperCase();
        if (device != null) {
            device.setCurrentValue(state);
            devicesRepo.save(device);

            DevicesValues values = new DevicesValues();
            values.setDevice(device);
            values.setSensorValue(state);
            values.setUpdateTime(new Date());
            devicesValuesRepo.save(values);
            return true;
        }
        return false;
    }
}
