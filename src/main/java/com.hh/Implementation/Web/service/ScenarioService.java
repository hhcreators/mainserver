package com.hh.Implementation.Web.service;

import com.hh.Implementation.DAO.entity.*;
import com.hh.Implementation.DAO.repos.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.management.Sensor;

import java.util.*;

@Service
public class ScenarioService {

    private final StationsRepo stationsRepo;
    private final DevicesRepo devicesRepo;
    private final IFTTTRepo iftttRepo;
    private final PresentedDevicesValuesRepo presentedDevicesValuesRepo;
    private final PresentedDevicesRepo presentedDevicesRepo;

    @Autowired
    public ScenarioService(StationsRepo stationsRepo,
                           DevicesRepo devicesRepo,
                           IFTTTRepo iftttRepo,
                           PresentedDevicesValuesRepo presentedDevicesValuesRepo,
                           PresentedDevicesRepo presentedDevicesRepo) {
        this.stationsRepo = stationsRepo;
        this.devicesRepo = devicesRepo;
        this.iftttRepo = iftttRepo;
        this.presentedDevicesValuesRepo = presentedDevicesValuesRepo;
        this.presentedDevicesRepo = presentedDevicesRepo;
    }


    public Map<String, Object> showMyScenarios(User user) {
        Map<String, Object> model = new HashMap<>();
        List<Station> stations = stationsRepo.findByUserId(user.getId());
        List<Device> devices = (List<Device>) devicesRepo.findAll();
        List<Device> devicesTemp = new ArrayList<>();
        List<IFTTT> ifttts = new ArrayList<>();
        for (Station s : stations) {
            for (Device d : devices) {
                if (s.getId().equals(d.getStation().getId())) {
                    devicesTemp.add(d);
                    ifttts.addAll(iftttRepo.findByThisDeviceId(d.getId()));
                }
            }
        }
        model.put("devices", devicesTemp);
        model.put("ifttts", ifttts);
        return model;
    }

    public Map<String, Object> showCreationForm(User user) {
        Map<String, Object> model = new HashMap<>();
        List<Station> stations = stationsRepo.findByUserId(user.getId());
        List<PresentedDevicesValues> presentedDevicesValues = (List<PresentedDevicesValues>) presentedDevicesValuesRepo.findAll();
        List<PresentedDevicesValues> presentedDevicesValuesSensors = new ArrayList<>();
        List<PresentedDevicesValues> presentedDevicesValuesControllers = new ArrayList<>();
        List<Device> devices = new ArrayList<>();
        List<Device> sensors = new ArrayList<>();
        List<Device> controllers = new ArrayList<>();
        for (Station s : stations) {
            devices.addAll(devicesRepo.findByStationId(s.getId()));
        }
        for (Device d : devices){
            if (d.getPresentedDevices().getDeviceType().equals("SENSOR")){
                sensors.add(d);
                for (PresentedDevicesValues pdv : presentedDevicesValues){
                    if (pdv.getPresentedDevices().getId().equals(d.getPresentedDevices().getId())){
                        presentedDevicesValuesSensors.add(pdv);
                    }
                }
            }
            if (d.getPresentedDevices().getDeviceType().equals("CONTROLLER")){
                controllers.add(d);
                for (PresentedDevicesValues pdv : presentedDevicesValues){
                    if (pdv.getPresentedDevices().getId().equals(d.getPresentedDevices().getId())){
                        presentedDevicesValuesControllers.add(pdv);
                    }
                }
            }
        }
        model.put("sensors", sensors);
        model.put("controllers", controllers);
        model.put("presentedValuesSensors", presentedDevicesValuesSensors);
        model.put("presentedValuesControllers", presentedDevicesValuesControllers);
        return model;
    }

    public boolean createScenario(String name,
                                  Device thisDevice,
                                  String thisValue,
                                  Device thatDevice,
                                  String thatValue) {
        IFTTT ifttt = new IFTTT();
        ifttt.setIftttName(name);
        ifttt.setThisDevice(thisDevice);
        ifttt.setThisValue(thisValue);
        ifttt.setThatDevice(thatDevice);
        ifttt.setThatValue(thatValue);
        iftttRepo.save(ifttt);
        return true;
    }



    public Map<String, Object> showEditForm(User user, Long iftttId) {
        Map<String, Object> model = new HashMap<>();
        Optional<IFTTT> ifttt = iftttRepo.findById(iftttId);
        List<PresentedDevicesValues> presentedDevicesValues = (List<PresentedDevicesValues>) presentedDevicesValuesRepo.findAll();
        List<Station> stations = stationsRepo.findByUserId(user.getId());
        List<Device> devices = (List<Device>) devicesRepo.findAll();
        List<PresentedDevicesValues> presentedDevicesValuesSensors = new ArrayList<>();
        List<PresentedDevicesValues> presentedDevicesValuesControllers = new ArrayList<>();
        List<Device> sensors = new ArrayList<>();
        List<Device> controllers = new ArrayList<>();
        List<IFTTT> ifttts = new ArrayList<>();

        if (ifttt.isPresent()) {
            for (Station s : stations) {
                for (Device d : devices) {
                    if (s.getId().equals(d.getStation().getId())) {
                        ifttts.addAll(iftttRepo.findByThisDeviceId(d.getId()));
                        Optional<PresentedDevices> presentedDevice = presentedDevicesRepo.findById(d.getPresentedDevices().getId());
                        if (presentedDevice.isPresent()) {
                            if (presentedDevice.get().getDeviceType().equals("SENSOR")) {
                                sensors.add(d);
                                for (PresentedDevicesValues pv : presentedDevicesValues) {
                                    if (pv.getPresentedDevices().getId().equals(presentedDevice.get().getId())) {
                                        presentedDevicesValuesSensors.add(pv);
                                    }
                                }
                            }
                            if (presentedDevice.get().getDeviceType().equals("CONTROLLER")) {
                                controllers.add(d);
                                for (PresentedDevicesValues pv : presentedDevicesValues) {
                                    if (pv.getPresentedDevices().getId().equals(presentedDevice.get().getId())) {
                                        presentedDevicesValuesControllers.add(pv);
                                    }
                                }
                            }

                        }
                    }
                }
            }
            model.put("presentedValuesSensors", presentedDevicesValuesSensors);
            model.put("presentedValuesControllers", presentedDevicesValuesControllers);
            model.put("devices", devices);
            model.put("sensors", sensors);
            model.put("controllers", controllers);
            model.put("ifttts", ifttts);
            model.put("iftttT", ifttt.get());
        }
        return model;
    }

    public boolean updateScenario(Long iftttId,
                                  String scenarioName,
                                  Device scenarioThisDevice,
                                  String scenarioThisValue,
                                  Device scenarioThatDevice,
                                  String scenarioThatValue) {
        Optional<IFTTT> iftttOptional = iftttRepo.findById(iftttId);
        if (iftttOptional.isPresent()){
            IFTTT iftttTemp = iftttOptional.get();
            iftttTemp.setIftttName(scenarioName);
            iftttTemp.setThisDevice(scenarioThisDevice);
            iftttTemp.setThisValue(scenarioThisValue);
            iftttTemp.setThatDevice(scenarioThatDevice);
            iftttTemp.setThatValue(scenarioThatValue);
            iftttRepo.save(iftttTemp);
            return true;
        }
        return false;
    }

}
