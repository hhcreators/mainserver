package com.hh.Implementation.Web.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/registration").setViewName("registration");
        registry.addViewController("/storage").setViewName("storage");
        registry.addViewController("/stations").setViewName("stations");
        registry.addViewController("/sensors").setViewName("sensors");
        registry.addViewController("/controllers").setViewName("controllers");
        registry.addViewController("/deviceList").setViewName("deviceList");
        registry.addViewController("/deviceValuesList").setViewName("deviceValuesList");

    }

}