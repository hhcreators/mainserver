package com.hh.Interfaces;

import org.java_websocket.WebSocket;
import org.json.JSONObject;

public interface MessageHandlerInt {

    boolean processMessage(String message, WebSocket socket);
}
