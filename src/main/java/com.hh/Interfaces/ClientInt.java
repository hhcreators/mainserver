package com.hh.Interfaces;

import java.net.InetSocketAddress;

public interface ClientInt {

    InetSocketAddress getInetSocketAddress();

    String getIP();

    long getID();

    boolean setID(long id);

    void closeConnection();
}
