package com.hh;

import org.java_websocket.WebSocketImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeServer {
    public static void main(String[] args){
        try {
            WebSocketImpl.DEBUG = false;
            SpringApplication.run(HomeServer.class);

        } catch (Exception e) {
            System.err.println(e + " Port is busy, check connections");
        }
    }
}
