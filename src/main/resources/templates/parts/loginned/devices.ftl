<#macro my_stations>
    <#if stations?hasContent>
<table class="mx-auto table mt-4">
    <thead class="thead-light">
    <th scope="col">ID</th>
    <th scope="col">Station type</th>
    </thead>
<tbody>
        <#list stations as station>
                <tr>
                    <td >${station.id?ifExists}</td>
                    <td >
                        ${station.stationType?ifExists}
                    </td>
                </tr>
        </#list>
</tbody>
</table>
    <#else>
    <p>You have no stations !</p>
    </#if>
</#macro>

<#macro new_station>
    <div>
        Add new station :
        <form method="post" action="new_station">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />﻿
            <input type="number" name="station_id" placeholder="Station ID" />
            <input type="text" name="stationPsw" placeholder="Station password" />
            <button type="submit" class="btn btn-primary ml-2">Submit</button>
        </form>
    </div>
</#macro>



<#macro my_sensors>
    <#if sensors?hasContent>
        <table class="mx-auto table mt-4">
            <thead class="thead-light">
            <th scope="col">ID</th>
            <th scope="col">Sensor type</th>
            <th scope="col">Sensor name</th>
            <th scope="col">Sensor value</th>
            </thead>
            <tbody>
                <#list sensors as sensor>
                            <tr>
                                <td >${sensor.id?ifExists}</td>
                                <td >${sensor.presentedDevices.deviceType?ifExists}</td>
                                <td >${sensor.deviceName?ifExists}</td>
                                <td >${sensor.currentValue?ifExists}</td>
                            </tr>
                </#list>
            </tbody>
        </table>
    <#else>
        <p>You have no sensors !</p>
    </#if>
</#macro>

<#macro my_controllers>
    <#if controllers?hasContent>
        <table class="mx-auto table mt-4">
            <thead class="thead-light">
            <th scope="col">ID</th>
            <th scope="col">Controller type</th>
            <th scope="col">Controller name</th>
            <th scope="col">Controller value</th>
            </thead>
            <tbody>
                <#list controllers as messageService>
                <tr>
                    <td >${messageService.id?ifExists}</td>
                    <td >${messageService.presentedDevices.deviceType?ifExists}</td>
                    <td >${messageService.deviceName?ifExists}</td>
                    <td >${messageService.currentValue?ifExists}</td>
                </tr>
                </#list>
            </tbody>
        </table>
    <#else>
    <p>You have no controllers !</p>
    </#if>
</#macro>