<#macro devicesValue>
    <#if devices?hasContent>
    <div class="d-flex flex-wrap justify-content-center">
    <#list devices as device>
        <div class="card text-center ml-2 mr-2 mt-4" style="width: 25rem;">
            <div class="card-body">
                <h5 class="card-title">${device.deviceName?ifExists}</h5>
                <p class="card-text">${device.presentedDevices.deviceSubType?ifExists}</p>
                <p>
                    <#if device.currentValue?contains("ON")>
                        <a href="#" class="btn btn-success">ON</a>
                        <a href="/devicesValues/changeState?state=off&device=${device.id}" class="btn btn-outline-danger">OFF</a>
                    <#elseIf device.currentValue?contains("OFF")>
                        <a href="/devicesValues/changeState?state=on&device=${device.id}" class="btn btn-outline-success">ON</a>
                        <a href="#" class="btn btn-danger">OFF</a>
                    <#else >
                        ${device.currentValue?ifExists}
                    </#if>
                </p>
                <a href="/devicesValues/detail/${device.id}" class="btn btn-primary">Details</a>
            </div>
        </div>
    </#list>
    </div>
    <#else>
            <#--Include main page-->
    </#if>
</#macro>

<#macro detail>
    <#if deviceValues?hasContent>
            <table class="mx-auto table mt-4">
                <thead class="thead-light">
                <th scope="col">Device id</th>
                <th scope="col">Device name</th>
                <th scope="col">Device value</th>
                <th scope="col">Update time</th>
                </thead>
                <tbody>
                <#list deviceValues?sortBy("updateTime")?reverse as value>
                    <tr>
                        <td >${value.device.id?ifExists}</td>
                        <td >${deviceName?ifExists}</td>
                        <td >${value.sensorValue?ifExists}</td>
                        <td >${value.updateTime?ifExists}</td>
                    </tr>
                    </#list>
                </tbody>
            </table>
    </#if>
</#macro>

<#macro ifttt>
<a href="/scenario/new" class="btn btn-primary">New</a>
    <#if ifttts?hasContent>
    <div class="d-flex flex-wrap justify-content-center">
    <#list ifttts as ift>
        <div class="card text-center ml-2 mr-2 mt-4" style="width: 25rem;">
            <div class="card-body">
                <h5 class="card-title">${ift.iftttName?ifExists}</h5>
                <p class="card-text">Scenario</p>
                <p>

                    <#list devices as dv>
                        <#if dv.id == ift.thisDevice.id>
                            If
                            ${dv.deviceName}
                        </#if>
                    </#list>
                    <#list devices as dv>
                        <#if dv.id == ift.thisDevice.id>
                            has got
                            ${ift.thisValue}
                        </#if>
                    </#list>

                    <#list devices as dv>
                        <#if dv.id == ift.thatDevice.id>
                            then
                            ${dv.deviceName}
                        </#if>
                    </#list>
                    <#list devices as dv>
                        <#if dv.id == ift.thatDevice.id>
                            will be
                            ${ift.thatValue}
                        </#if>
                    </#list>
                </p>
                <a href="/scenario/edit/${ift.id}" class="btn btn-primary">Edit</a>
            </div>
        </div>
    </#list>
    </div>
    <#else>
    </#if>
</#macro>

<#macro iftttNew>
<form method="post" action="/scenario/new/create">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />﻿
    <div class="form-group">
        <label for="exampleInputPassword1">Scenario name</label>
        <input type="text" name="scenarioName" class="form-control" id="exampleInputPassword1" placeholder="Name">
    </div>
    <div class="form-group">
        If device
      <select name="scenarioThisDevice" class="custom-select" onclick="disableNoRequiredFF(this.value);">
          <option value="0" selected>Choose...</option>
          <#if sensors?hasContent>
              <#list sensors as sens>
                  <option value="${sens.id}">${sens.deviceName}</option>
              </#list>
          </#if>
      </select>
    </div>
    <div class="form-group">
        has got
        <select name="scenarioThisValue" class="custom-select">
            <option selected>Choose...</option>
            <#if presentedValuesSensors?hasContent>
                <#list presentedValuesSensors as pv>
                    <#list sensors as sens>
                        <#if sens.presentedDevices.id == pv.presentedDevices.id>
                            <option class="toDisableFF ${sens.id}" value="${pv.deviceValue}" style="display: none">${pv.deviceValue}</option>
                        </#if>
                    </#list>
                </#list>
            </#if>
        </select>
    </div>
    <div class="form-group">
        then that device
        <select name="scenarioThatDevice" class="custom-select" onclick="disableNoRequiredSF(this.value)">
            <option value="0" selected>Choose...</option>
            <#if controllers?hasContent>
                <#list controllers as cont>
                    <option value="${cont.id}">${cont.deviceName}</option>
                </#list>
            </#if>
        </select>
    </div>
    <div class="form-group">
        make
        <select name="scenarioThatValue" class="custom-select">
            <option selected>Choose...</option>
            <#if presentedValuesControllers?hasContent>
                <#list presentedValuesControllers as pv>
                    <#list controllers as cont>
                        <#if cont.presentedDevices.id == pv.presentedDevices.id>
                            <option class="toDisableSF ${cont.id}" value="${pv.deviceValue}" style="display: none">${pv.deviceValue}</option>
                        </#if>
                    </#list>
                </#list>
            </#if>
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
        <script>
            function disableNoRequiredFF(uid) {
                console.log(uid);
                var toDis = document.getElementsByClassName("toDisableFF");
                var toOn = document.getElementsByClassName(uid);

                Array.prototype.forEach.call(toDis, function(el) {
                    el.style.display='none';
                });

                Array.prototype.forEach.call(toOn, function(el) {
                    el.style.display='';
                });
            }
            function disableNoRequiredSF(uid) {
                var toDis = document.getElementsByClassName("toDisableSF");
                var toOn = document.getElementsByClassName(uid);

                Array.prototype.forEach.call(toDis, function(el) {
                    el.style.display='none';
                });

                Array.prototype.forEach.call(toOn, function(el) {
                    el.style.display='';
                });
            }
        </script>
</#macro>


<#macro iftttEdit>
    <form method="post" action="/scenario/edit/${iftttT.id?ifExists}">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />﻿
        <input type="hidden" name="iftttId" value="${iftttT.id}" />﻿

        <div class="form-group">
            <label for="exampleInputPassword1">Scenario name</label>
            <input type="text" name="scenarioName" class="form-control" value="${iftttT.iftttName}">
        </div>
        <div class="form-group">
            If device
            <select name="scenarioThisDevice" class="custom-select" onclick="disableNoRequiredFF(this.value)">
                <#if sensors?hasContent>
                    <#list sensors as dv>
                        <#if dv.id == iftttT.thisDevice.id>
                            <option value="${dv.id}" selected>
                                ${dv.deviceName}
                            </option>
                        </#if>
                    </#list>
                    <#list sensors as sens>
                        <#if sens.id != iftttT.thisDevice.id>
                        <option value="${sens.id}">
                            ${sens.deviceName}
                        </option>
                        </#if>
                    </#list>
                </#if>
            </select>
        </div>
        <div class="form-group">
            has got
            <select name="scenarioThisValue" class="custom-select" >

            <#if presentedValuesSensors?hasContent>
                <#list presentedValuesSensors as pv>
                    <#list sensors as sens>
                        <#if sens.presentedDevices.id == pv.presentedDevices.id>
                            <#if pv.deviceValue == iftttT.thisValue>
                                <option class="toDisableFF ${sens.id}" value="${pv.deviceValue}" selected>${pv.deviceValue}</option>
                            <#else >
                                <option class="toDisableFF ${sens.id}" value="${pv.deviceValue}">${pv.deviceValue}</option>
                            </#if>
                        </#if>
                    </#list>
                </#list>
            </#if>
            </select>
        </div>
        <div class="form-group">
            then that device
            <select name="scenarioThatDevice" class="custom-select" onclick="disableNoRequiredSF(this.value)">
                <#if controllers?hasContent>
                    <#list controllers as dv>
                        <#if dv.id == iftttT.thatDevice.id>
                            <option value="${dv.id}" selected>
                                ${dv.deviceName}
                            </option>
                        </#if>
                    </#list>
                    <#list controllers as dv>
                        <#if dv.id != iftttT.thatDevice.id>
                        <option value="${dv.id}">
                            ${dv.deviceName}
                        </option>
                        </#if>
                    </#list>
                </#if>
            </select>
        </div>
        <div class="form-group">
            will be
            <select name="scenarioThatValue" class="custom-select">
            <#if presentedValuesControllers?hasContent>
                <#list presentedValuesControllers as pv>
                    <#list controllers as cont>
                        <#if cont.presentedDevices.id == pv.presentedDevices.id>
                            <#if pv.deviceValue == iftttT.thatValue>
                                <option class="toDisableSF ${cont.id}" value="${pv.deviceValue}" selected>${pv.deviceValue}</option>
                            <#else >
                                <option class="toDisableSF ${cont.id}" value="${pv.deviceValue}">${pv.deviceValue}</option>
                            </#if>
                        </#if>
                    </#list>
                </#list>
            </#if>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <script>
            function disableNoRequiredFF(uid) {
                var toDis = document.getElementsByClassName("toDisableFF");
                var toOn = document.getElementsByClassName(uid);

                Array.prototype.forEach.call(toDis, function(el) {
                    el.style.display='none';
                });

                Array.prototype.forEach.call(toOn, function(el) {
                    el.style.display='';
                });
            }
            function disableNoRequiredSF(uid) {
                var toDis = document.getElementsByClassName("toDisableSF");
                var toOn = document.getElementsByClassName(uid);

                Array.prototype.forEach.call(toDis, function(el) {
                    el.style.display='none';
                });

                Array.prototype.forEach.call(toOn, function(el) {
                    el.style.display='';
                });
            }
                <#list devices as dv>
                    <#if dv.id == iftttT.thisDevice.id>
                            disableNoRequiredFF(${dv.id});
                    </#if>
                    <#if dv.id == iftttT.thatDevice.id>
                            disableNoRequiredSF(${dv.id});
                    </#if>
                </#list>
        </script>
    </form>

</#macro>