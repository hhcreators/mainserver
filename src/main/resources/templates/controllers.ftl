<#import "parts/common.ftl" as comm>
<#import "parts/loginned/devices.ftl" as dv>

<@comm.main_wrap>
    <@dv.my_controllers/>
</@comm.main_wrap>
