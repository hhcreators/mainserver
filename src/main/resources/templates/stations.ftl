<#import "parts/common.ftl" as comm>
<#import "parts/loginned/devices.ftl" as st>

<@comm.main_wrap>
    <@st.new_station/>
    <@st.my_stations/>
</@comm.main_wrap>
